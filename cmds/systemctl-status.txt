● mpc-live
    State: running
     Jobs: 0 queued
   Failed: 0 units
    Since: Wed 2018-01-31 21:26:27 CET; 10min ago
   CGroup: /
           ├─init.scope
           │ └─1 /sbin/init
           └─system.slice
             ├─dbus.service
             │ ├─364 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation
             │ └─512 /usr/libexec/polkitd --no-debug
             ├─udisks.service
             │ ├─507 /usr/libexec/udisks-daemon --no-debug
             │ └─508 udisks-daemon: not polling any device
             ├─wpa_supplicant.service
             │ └─452 /usr/sbin/wpa_supplicant -u
             ├─system-btattach.slice
             │ └─btattach@ttyS0.service
             │   └─372 /usr/bin/btattach -B /dev/ttyS0 -P bcm
             ├─system-serial\x2dgetty.slice
             │ └─serial-getty@ttyS2.service
             │   └─379 /sbin/getty -L ttyS2 115200 vt100
             ├─systemd-journald.service
             │ └─184 /lib/systemd/systemd-journald
             ├─systemd-resolved.service
             │ └─445 /lib/systemd/systemd-resolved
             ├─az01-usbsata-fixer.service
             │ └─377 /usr/bin/az01-usbsata-fixer --firmware /lib/firmware/jms578_std_v00.01.00.06_self_power.bin
             ├─connman.service
             │ └─419 /usr/sbin/connmand -n
             ├─systemd-logind.service
             │ └─370 /lib/systemd/systemd-logind
             ├─sshd.service
             │ ├─458 /usr/sbin/sshd -D -e
             │ ├─537 sshd: root@pts/0       
             │ ├─539 -sh
             │ └─552 systemctl status
             ├─systemd-udevd.service
             │ └─213 /lib/systemd/systemd-udevd
             ├─az01-power-button.service
             │ └─385 /usr/bin/az01-pwrbtn
             ├─az01-script-runner.service
             │ └─425 /usr/bin/az01-script-runner
             ├─bluetooth.service
             │ └─375 /usr/libexec/bluetooth/bluetoothd
             └─inmusic-mpc.service
               ├─434 /bin/sh /usr/bin/az01-launch-MPC
               ├─451 systemd-inhibit --what=handle-power-key /usr/bin/MPC
               └─456 /usr/bin/MPC
