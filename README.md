% MPC Live Internals
% by rvense

Introduction
----

The [Akai MPC Live](http://akaipro.com/products/mpc-series/mpc-live) is a musical instrument released in 2017. It is a sampler, sequencer, and effects box, the latest of a series of "Music Production Studios" that started in the 1980's. The MPC machines have played a defining role in house and hip-hop production, and also been very popular in other genres of electronic music.

The latest MPCs (the MPC Live and MPC X) are the first in a number of years to run without being tethered to a PC or Mac. They are standalone like the classics in the series, but present much more modern, touchscreen-based user interfaces, supplemented by hardware buttons to naviage the software, in addition to the defining feature of the MPCs, the 4x4 grid of pressure-sensitive pads used for finger drumming.

The two new MPCs also have modern connectivity, like support for high-capacity SD cards, an internal SATA drive, USB sticks and MIDI controllers, as well as WiFi and Bluetooth. The software they run looks and feels like a derivative of Akai's new PC-based MPC software, and they support switching between the standalone mode, and a "controller mode" when connected to a PC. 

In controller mode, the software runs on the PC instead of the box itself, but still allows the use of the box to navigate the software, and also streams a view of the data from the PC software to the touchscreen on the external device. With a few limitations, the PC version of the software and the standalone box can share projects and files. In controller mode, the box appears as a USB class-compliant MIDI and audio interface for use with any DAW, as well as access to the internal storage over USB.

It comes as no surprise, of course, that the stand-alone boxes are, in fact, computers themselves. The Live and X were first revealed to the world through [FCC compliance filings](https://fccid.io/Y4O-ACV8) (because of the WiFi), and a lot of details were easily deducible from these filings.

In addition to the FCC filings, the below is based on disassembling my own MPC Live, and inspecting the running software through the serial port on the mainboard. There's a lot of the hardware I've yet to explore, and I obviously need to expand this with pictures, so expect updates to this document. I would also love to look inside an MPC X or Touch, but I don't have access to either.

Fingerpokers beware
----

If you're reading this, chances are you have an MPC Live yourself. Maybe this document will inspire you to take it apart and have a poke. 

Don't do that unless you know what you're doing. 

I have to commend Akai for the design of the Live hardware, it is very accessible, a lot more than these things often are nowadays, but poking inside it still comes with the risk of ruining your 1,000 euro toy. In which case your dog will likely think less of you, your significant other will roll their eyes at your incompetence, and, crucially, Akai Technical Support will likely reply to any requests for help with a "Too bad, nerd". 

I, being a nice guy, will be happy to offer my sympathies and best wishes because I do know how it feels to ruin something by being curious to it, but it goes without saying that I am not responsible for what you do to your stuff. Just because it worked for me once, doesn't mean it will work for you once, or for me tomorrow. 

The MPC Live has no mains voltage inside, but it does contain a lithium battery that has a number of interesting failure modes if mistreated, including exploding and catching fire. Be careful.

Hardware overview
----

:::: {.picture-thumbnail}
[High-resolution picture of the internals.](pics/internals.jpg)
::::

The Live contains a number of boards inside: the largest are the mainboard and the audio/midi board. The SD card reader is on a separate board which connects to the mainboard, and there's a small board related to the touch screen control. The control surface consists of three boards: the pads themselves, including the row of buttons above are on one; the encoders to the right of the display on another; and the buttons underneath the display on a third. 

Also visible is the battery. It's connected to the mainboard by 8 wires. I'm wary to disconnect it from the mainboard, since I've heard that some battery controller chips don't like that, and it's not immediately clear if the charging circuit is on the mainboard or in the battery pack.

The boards are connected with either flat flex cables or wires with decent headers on them. I've still not taken mine apart completely, but it looks simple enough as far as these go. The interior in general is tidy, there's no glue or similar to be seen, everything is servicable, and matches the Live's exterior feeling of quality.

A note: While the box says "Akai" on the outside, the boards are mostly marked with the brand of Akai's parent company, InMusic. The firmware also makes references to the "InMusic MPC".

The mainboard
----

The MPC Live and X hardware is centered around the Rockchip RK3288 system-on-chip, which contains a quad-core ARM Cortex A17 and a number of peripherals. The FCC filings and device tree used by the Linux software refer to the Radxa Rock2 system-on-module, a board which contains the RK3288 itself, 2GB RAM, eMMC storage for the system software, as well as power management. The silkscreening on my board indicates it was revised after the FCC filings. There's a heatsink on the board that covers the SOM, I've not taken it off to see if it actually contains a Radxa board or some redesign, but the basic principle looks similar to the photos in the FCC documents. Linux only reports 8GB internal storage (eMMC), but the Rock2 was not available in this configuration..

The SOM is attached to a "base board", or mainboard, which is InMusic's design. This board is the left-most third of the unit when looking from the back. It contains the power button, the plug for the external DC supply, and the USB ports (both host and device ports). The display is attached here (using MIPI), as is the controller that handles the touch input. The base board also contains two unpopulated ports near the front that appear to be HDMI video and gigabit ethernet. These were likely used during development. The RK3288's HDMI is still mentioned in Linux's device tree, but marked as disabled.

All the power is handled here. The 19V power supply connects here, and there is some circuitry as well as a connection to the battery pack. The audio/midi board is powered over the internal USB connections, and this in turn powers the other control surface-related boards.

The base board contains an internal micro-SD card reader, in addition to the external, full-size one which is connected via a USB header. The internal reader in mine has a 16GB card in it. This is the "internal storage" that the factory content is on. This is in addition to the eMMC, which is only used for system software. The base board also has a SATA connector, and a SATA-USB bridge - the RK3288 doesn't have real SATA.

Wifi and bluethooth are handled by a single module, with a single cable to an external aerial. 

On the far side of the board, away from the external ports, there is a debug port. On my board it was already populated. It is a 3.3V UART, the pinout is as follows:

    GND
    RX
    TX
    3.3V

With GND being closest to the internal SD card reader.

There is a button called "recovery" on the left side board. Pressing it while booting puts the Live into the same update/recovery mode that can be reached from software. The right side of the board has a reset button, next to an unpopulated two-pin header labeled "latch".

All the knobs, pads and buttons are connected to the audio/midi board, which connects to the mainboard via two internal USB connections (using plain cables and pin-headers, but clearly marked as USB on both sides). On the mainboard side, these connections are marked "USB hub", because they go to a Microchip USB5537 hub that also handles the external SD card reader and the two external device ports. There is an internal pin header labeled "USB host", unconnected.

In addition to the USB connections, there's also a flat-flex cable (FFC) between the mainboard and the audio/midi board. Silkscreening suggests, among other things, a UART connection to the STM32 microcontroller that handles the MIDI and control surface. The output of the lsof utility doesn't mention any serial ports in use, though, (except for the console), so it's probably not used in normal operation.

The mainboard sees the audio/midi controller board as class-compliant audio and MIDI devices. All the buttons and knobs on the front panel simply send MIDI information to the MPC software.

The audio/midi control board
----

This board contains the MIDI jacks, the audio jacks, and the gain pots. It takes up about two thirds of the machine's width, but doesn't extend as far back as the base board.

As noted, it implements two USB devices, one for the MIDI and control surface, one for the audio. It contains flat-flex cables to the boards with knobs and switches on them. Most connectors on this board are helpfully labelled in the silk screening! 

The board contains four larger ICs, in additional to all the requisite opamps etc.:

* One, near the MIDI sections, is an STM32F401. This is an ARM-based microcontroller with built-in USB and a number of UARTs. It would be suitable for implementing a USB MIDI interface, so that's probably what it's doing. Most likely it's also handling all the knobs, front panel switches, and the pads. As stated above, silkscreening on the board suggests a UART connection to the mainboard, but I've not pinned down what for, exactly.

* The second, near the audio section, is an XMOS controller marked "6U6C5". These are commonly used in USB audio interfaces, not least because the manufacturer provide much of the required code for a USB 2.0 audio interface as reference code. As far as I can tell, the markings map to part number XS1-U6A-64-FB96. 

* The last two are an AKM AK4621 CODEC, which handles the input and the main output; and an AK4413 4-channel DAC, which give the extra pairs. The two chips have slightly different characteristics, and, indeed, the specs in the MPC manual also provides different specs for the different pairs.

In a subfolder of /usr/share/Akai, we find files to upgrade the control surface. There's a small shellscript that plays a MIDI SysEx file at MIDI 2, which is the port that control surcace (knobs, switches) transmits on. Comments in the script suggests this switches the device to DFU (Device Firmware Upgrade) mode. It then uses dfu-util to install the actual firmware.

Pictures and more info to be added!

The software
----

The serial port provides access to the console (115200 baud). On boot, we first get the familiar uBoot bootlader and quickly thereafter Linux boots from eMMC. You can log in as root, no password needed.

The software is a very straight-forward modern Linux system based on the [buildroot project](https://buildroot.org). It contains a busybox shell and uses systemd for its init system. The device trees used to boot and configure the system are in /boot, but unfortunately InMusic don't bundle the kernel config in /proc. Maybe they'll provide it if someone asks them nicely. Since the hardware is mostly connected via USB, there isn't really much to the device tree, and the kernel configuration is likely very straight-forward.

MPC Live firmware 2.07 is based on buildroot version 16.02, glibc 2.22 and Linux 4.4.80, and was compiled with gcc 5.3.0. Firmware 2.1 is based on buildroot 2017.08, Linux 4.9.77, and glibc 2.25, compiled with gcc 6.3.0. That means 2.1 has the patches that mitigate Meltdown and Spectre, BTW - AFAICT before my phone did.

The main application is /usr/bin/MPC. This appears to be basically the only program the user interacts with directly, it takes over the entire screen (using the framebuffer). There are a few inMusic-provided scripts here and there in the filesystem, but mostly it's just this one binary. It seems to use the JUCE library and talk to the audio and MIDI interfaces via the standard Linux ALSA audio system. Being closed-source, it is likely to remain a black box and its exact relation to Akai's PC desktop software can only be guessed at.

The root filesystem (ext4, from the internal eMMC) is mounted to / read-only by default. You can remount it read-writable and e.g. add your ssh key to /root/.ssh/authorized\_keys. SSH is included but not started by default. For some reason (I'm sure it's used somewhere), /etc is remounted by systemd as an overlayfs. This means that even if you remount as read/write, changes in /etc won't be reflected before the end of the boot process, so dis/enabling systemd units will appear not to work unless you unmount the overlayfs first. I.e., to make sshd start on boot, log in over the serial console and do:

    # mount / -o rw,remount
    # umount /etc
    # systemctl enable sshd

The main application is started by the systemd unit inmusic-mpc. You can stop this or disable it if you like, in which case your MPC will boot to a blank screen.

Memory usage 
----

*First experiment (18th december 2019)*

There's a lot of chatter about how much memory the MPCs have and how much is usable for samples. The machines have 2GB for everything. As of firmware 2.7, there's 1330 MB free when my Live boots up (according to the command 'free').

If I then load a 54 MB soundfile (5 minutes of stereo sound 16/44.1), free reports 1150 MB free, or 180 MB less.

If I rename the loaded sample and load it again (so now 10 minutes of stereo sound in memory), I have 1039MB free, or 110 MB less. A third instance, 930 MB free. And so on: 821, 712, 603, 494, 386, and then 275, at which point I got a low memory warning. One more load brought me to 165 MB free, and then I was not allowed to load further samples. Switching to the sampler, I was warned that my max sampling time had been reduced to 3 minutes - so I'm guessing MPC insists on there being about 100 MB free. 

At this point I was not able to create new tracks. After deleting a single sample (bringing me back to about 270 MB free), I was able to create 8 audio tracks and put an instance of the sample as well as an AIR Delay effect on all of them.

Except for the first loaded sample, this means it's approximately 110 MB to hold a sample that's 54 MB on disk in CD quality. This is consistent with the rumours that the MPC loads sounds as 32 bit samples. 

All in all, I was able to load about 45 minutes of audio data and still apparently use the MPC as normal. The results might be different with smaller samples or situations that lead to memory fragmentation - where you don't load the samples at once but load, then delete, then load, or do a lot of recording and bouncing.

Controller mode
----

One part of what controller mode does is simply to redirect this USB hub to the host computer, so that the two devices in the control board get attached to the computer, as is the card reader and the internal SATA drive.

There is a script (/bin/az01-usbmux-switch) which controls usb multiplexor to switch what the hub is connected to: in standalone mode, the hub is connected to the RK3288; in controller mode, they're detached and switched to connect to the host computer - that is, when running in controller mode, the audio and control surface don't reach the RK3288 at all.

This script also flips a GPIO which apparently resets the SATA bridge, to make it aware that it is now attached to another computer.

As far as I can tell, controller mode works by running the application in a specific mode and sending commands over USB. It does not use display streaming like the MPC Touch.

Running other software
----

I have not attempted to overwrite the system software on the eMMC, but information on how to do this for Radxa and Rockchip systems is readily available (see e.g. [Rockchip's wiki](http://opensource.rock-chips.com/wiki_Main_Page). Rebooting the Akai in update/recovery mode, either from the software or via the recovery button on the mainboard, puts uboot in "fastboot" mode.

The bootloader is uboot, reachable from the serial console. Its configuration is read from a small partition on eMMC. The configuration defines, in plain text, commands to boot the normal system and to fastboot (see the variables in dump linked below). 

Akai's updater uses the fastboot protocol over USB. Fastbook is a [very straight-forward text-based protocol](https://android.googlesource.com/platform/system/core/+/2ec418a4c98f6e8f95395456e1ad4c2956cac007/fastboot/fastboot_protocol.txt). [Wiresharking](https://wireshark.org) the 2.1 update procedure reveals the following sequence of commands from the updater application and replies from the bootloader:

    oem inmusic-unlock-magic-7de5fbc22b8c524e
    OKAY
    getvar:power-source
    OKAYac
    getvar:power-source
    OKAYac
    getvar:partition-type:rootfs
    OKAYext4
    erase:rootfs
    OKAY
    download:19000000
    DATA19000000
    ... bunch of data ...
    OKAY
    flash:rootfs
    OKAY 
    reboot
    OKAY

This looks like a completely standard fastboot update flow, except for the first command - which looks like a small InMusic addition to "lock" the bootloader. It is hard-coded in the second stage bootloader that's in partition 3 of the eMMC. The unlock command can be run from the standard fastboot command line utility; without it, some other flashing-relating commands respond with "permission denied", but after it, the commands appear succesful. 

See the section below for notes about Fastboot updates.

Another avenue for getting a different Linux on the Live would be to boot from a different device. Unfortunately, uboot does not appear to see the internal sd card reader, so it's not immediately clear to me if booting from that will require updating the bootloader, or merely some reconfiguration magic. It could also be that the partition table is not what it's expecting on my card, so it's ignoring it. The boot environment makes reference to two mmc devices, but mmcinfo in uboot only shows one. The external SD card reader is connected via USB and on the other side of a hub, but it might still be possible to get uboot to boot from it.

Radxa have their own ["rockusb"](http://opensource.rock-chips.com/wiki_Rockusb#Maskrom_mode) recovery system, but I've not been tried to get into it. Since it lies in ROM, however, and is what's booted when all else fails, it should be possible to get into it by erasing the eMMC - best to have a known way back first! This means, though, that once we're able to construct a full image for the internal eMMC (from scratch or by dumping and modifying what's already there), it will be possible to get a fully customized system image on there using Radxa's tools. The fastboot solution is a lot nicer, though, since it would allow firmware replacement without using the serial console to modify the system first, and without needing to touch the bootloader in any way.

I have tried to cross-compile statically linked binaries from my x86 box and they of course run fine. I have also successfully cross-compiled QT5 and run an example app on the framebuffer: ![QT5 Analog Clock](pics/qt5clock.jpg)

In general, it should be quite easy to compile regular Linux audio software for the Live and run it instead of the MPC application if someone wanted to do that, provided that they can run on the framebuffer. It should also run most e-mail clients adequately.

When will MPC Live JJOS come out?
----

This comes up often in various forms. Some earlier MPCs had a 3rd party operating system that added many more features, called JJOS, and a lot of people want something similar for the MPC Live. The short answer is: it won't.

The key thing to understand here is that the person who did JJOS had access to the source code for everything. It's not really a hack, just further development of the original firmware. This is not possible with what we have on the MPC Live/X.

The OS (in the strict meaning of the term) on the MPCs is Linux, what makes it an MPC is the application that runs on it. This application has the same relation to the OS that something like Cubase running on a computer has to Windows or macOS. Even if you had full access to the Windows source code and were able to replace it with what you like, you wouldn't be able to add many interesting new features to Cubase. The musically interesting parts of the MPC are all in this one application. We can replace it, but we can't really change it... A box of pd/Supercollider patches can be a beautiful thing, but Norns and Organelle are way ahead here, and if I wanted to write new software from scratch, I probably wouldn't target an expensive platform under someone else's control like the MPCs.

Commonly requested features like MIDI out over USB and multiple MIDI inputs, the lack of those are actually mostly related to the user interface. Other things are (better sampler features, disk streaming) pertain to the structure of Akai's application. None are in any way limited by the OS (in the strict sense of the term OS).

I hoped for a while it'd be feasible to run something alongside the MPC software, but unfortunately it takes over the screen completely, and getting anything else on there would mean killing the MPC software, kind of like quitting one desktop application and losing all your work to open another. It doesn't seem like a very promising endeavour, so I'm just using the MPC as it is right now. It's pretty good already.

NB: I am most happy to be proved wrong here!

Using fastboot to install updates
----

Akai's updater application for Mac has an "Update.img" image inside it. This file is the same as the image that can be copied to a USB device for updating the MPC without attaching it to a computer.

[binwalking](https://tools.kali.org/forensics/binwalk) earlier versions of the reveals an ext4 partition a few hundred bytes in that looks exactly like the rootfs which gets installed. 

As of version 2.6, the partition contained in the file is compressed. Binwalking it just revealed a bunch of compressed data. Cutting off the header (everything before the first xzipped section) gave a corrupt file. However, looking with a hexeditor, the last four bytes of the header, before the start of the first xzipped section binwalk found, was a number almost the same as the length of the file. It appears to be the length of the xzipped filesystem - by cutting off the header and everything after that length, I got an image that could be decompressed. *If you do this, make sure you can decompress and mount the filesystem before flashing it*.

I am able to flash the MPC from Linux by using the fastboot tool from the command line:

    $ fastboot oem inmusic-unlock-magic-7de5fbc22b8c524e
    $ fastboot flash rootfs mpc-rootfs-partition.img

I used a partition extract from the Update.img in the 2.1 version. *Do not use the Update.img file directly*. It needs to be just the partition, and the offset could depend on the version. In addition to the prospects of replacing Linux, this also means it's possible to install Akai's upgrades without a Mac or Windows computer. I have been doing all my updates this way since 2.2. I don't know if intermediate versions have changed the bootloader to make it impossible to still do, but theoretically it's something Akai could lock down if they wanted to. This methoad ensures that the bootloader does not get changed.

Outputs from various commands
----

Here are dumps of the output from various commands, as well as a boot log:

* [boot log, firmware 2.0.7](cmds/boot-log.txt)
* [boot log, firmware 2.1](cms/boot-log-21.txt)
* [mount: mounted filesystems](cmds/mount.txt)
* [annotated partition table of the internal eMMC](cmds/emmc-partitions.txt)
* [ps: running processes](cmds/psa.txt)
* [lsof: a list of open files](cmds/lsof.txt)
* [lsusb -v: attached usb devices](cmds/lsusb.txt)
* [systemctl status: all running systemd units](cmds/systemctl-status.txt)
* [environment variables set in uboot](cmds/boot-env.txt)
* [uboot mmcinfo](uboot-mmcinfo.txt)
* [uboot GPIO setup](uboot-gpio.txt)

About the author
----

My name is Niklas Nisbeth. I go by rvense and nn in various circles. I originally bought the Live to make music, something I used to do a lot. Unfortunately it seems like I've become too much of a Linux nerd to do that. 

I can be reached by sending e-mail to my first name @ my last name dot dk.

Revision history
----

* May 24, 2020: Remove Keybase from contact info since I deleted my account
* Dec 19, 2019: Tried to figure out how much memory is available for sample data
* Dec 7, 2019: Slight update about controller mode, added section about "JJOS Live"
* Jul 25, 2019: Expanded section about fastboot updates as of v2.6 - Time flies!
* Feb 20, 2018: QT5 example app running :)
* Feb 18, 2018: More information about the bootloader.
* Feb 11, 2018: Took it apart some more and added more hardware notes.
* Feb 4, 2018: More bootloader info and uboot command output. Unfortunately it looks like we can't boot from SD.
* Feb 3, 2018: Add dump of partition table from internal eMMC and updated info about the boot configuration.
* Jan 30, 2018: First cut.
