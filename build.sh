#!/bin/bash

set -euo pipefail

STYLE="style.css"
DATA_DIRS="tasteful-sprinkles.js pics cmds"
EXTRAS="-H headers.html -A footers.html" # can add headers and footers here with -H and -A
: ${OUTPUT_DIR:="."}
SOURCE=$1

echo outputting to $OUTPUT_DIR

pandoc \
  -s \
  --section-divs \
  --toc \
  -t html5 \
  $EXTRAS \
  -c $STYLE \
  -o $OUTPUT_DIR/index.html \
  $SOURCE

if [ $OUTPUT_DIR != "." ]; then
  cp $STYLE $OUTPUT_DIR/
  cp -a $DATA_DIRS $OUTPUT_DIR/
fi
