(function() {
  var ops = {
    ".picture-thumbnail": {
      init: function(e) {
        var links = e.querySelectorAll("a");
        links.forEach(function (l) {
          var [url, suffix] = (l.getAttribute("href") || "").split(".");

          if (suffix) {
            var thumb = document.createElement("img");
            thumb.setAttribute("src", url + "-thumb." + suffix);
            thumb.setAttribute("class", "thumbnail");
            l.prepend(thumb);
          }
        });
      }
    }
  };

  Object.keys(ops).forEach(function(k) {
    var elems = document.querySelectorAll(k);
    var init = ops[k].init;
    if (init) {
      elems.forEach(init);
    }
  });
}());
